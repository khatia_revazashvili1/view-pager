package Fragmentss

import android.annotation.SuppressLint
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.view.View

import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpager.R

class Fragment1: Fragment(R.layout.fragment_1) {


    private lateinit var butAdd: Button
    private lateinit var textView: TextView
    private lateinit var editTextNote: EditText



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)






        butAdd = view.findViewById(R.id.butAdd)
        textView = view.findViewById(R.id.textView)
        editTextNote = view.findViewById(R.id.editTextNote)

        val sharedPreferences = activity?.getSharedPreferences("MY_NOTES", MODE_PRIVATE)
        val text = sharedPreferences?.getString("Notes","")
        textView.text = text

        butAdd.setOnClickListener {
            val note = editTextNote.text.toString()

            val text = textView.text.toString()

            val result = note + "n" + text

            textView.text = result


            sharedPreferences?.edit()
                ?.putString("NOTE",result)
                ?.apply()


        }


    }
}


